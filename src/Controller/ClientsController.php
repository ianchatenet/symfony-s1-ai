<?php

namespace App\Controller;
use App\Repository\CustomerRepository;
use App\Repository\SaleProductRepository;
use App\Repository\SaleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Customer;
use App\Form\CustomerType;
use Knp\Component\Pager\PaginatorInterface;



class ClientsController extends AbstractController
{
    /**
     * @var CustomerRepository
     */
    private $repository;

    /**
     * @var SaleRepository
     */
    private $saleRepo;

    /**
     * @var SaleProductRepository
     */
    private $spRepo;

   

    public function __construct(CustomerRepository $repository, SaleRepository $saleRepo, SaleProductRepository $spRepo){
        $this->repository=$repository;
        $this->saleRepo=$saleRepo;
        $this->spRepo=$spRepo;
    }

    /**
     * @Route("/clients", name="clients")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $customers=$this->repository->findAll();

        $pagination = $paginator->paginate(
            $customers,
            $request->query->getInt('page', 1),
            10
        );
       
        
        return $this->render('clients/index.html.twig', [
            'controller_name' => 'ClientsController',
            "customers"=>$customers,
            "pagination"=>$pagination
        ]);
    }

    /**
     * @Route("/clients/{id}/show", name="client_show")
     */
    public function show($id)
    {
       
      
        $sales=$this->saleRepo->findBy(
            ['customer'=>$id]
        );

        return $this->render('clients/show.html.twig', [
            'controller_name' => 'ClientsController',
            "sales"=>$sales
        ]);
    }

    /**
     * @Route("clients/vente/{id}/detail", name="vente_detail")
     */
    public function salesDetail($id)
    {
        
      
        $spRepo=$this->spRepo->findBy(
            ['sale'=>$id]
        );

        dump($spRepo);
        return $this->render('clients/saleDetail.html.twig', [
            'controller_name' => 'ClientsController',
            "sales"=>$spRepo
        ]);
    }

    /**
     * @Route("/clients/new", name="client_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        
        $customer = new Customer();
        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($customer);
            $entityManager->flush();

            return $this->redirectToRoute('clients');
        }

        return $this->render('clients/new.html.twig', [
            'customer' => $customer,
            'form' => $form->createView(),
            'controller_name' => 'ClientsController'
        ]);
    }

    /**
     * @Route("/clients/{id}/edit", name="client_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Customer $customer): Response
    {

        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clients');
            
        }

        return $this->render('clients/edit.html.twig', [
            'customer' => $customer,
            'form' => $form->createView(),
            'controller_name' => 'ClientsController'
        ]);
    }


}
