<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductVariantRepository")
 */
class ProductVariant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductOptionValue")
     */
    public $productOptionValue_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     */
    private $product_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductOptionValueId(): ?productOptionValue
    {
        return $this->productOptionValue_id;
    }

    public function setProductOptionValueId(?productOptionValue $productOptionValue_id): self
    {
        $this->productOptionValue_id = $productOptionValue_id;
        return $this;
    }

    public function getProductId(): ?product
    {
        return $this->product_id;
    }

    public function setProductId(?product $product_id): self
    {
        $this->product_id = $product_id;
        return $this;
    }
}
