<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductOptionsRepository")
 */
class ProductOptions
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductOptionValue")
     * @ORM\JoinColumn(nullable=true)
     */
    private $productOptionValue_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProductOptionValueId(): ?productOptionValue
    {
        return $this->productOptionValue_id;
    }

    public function setProductOptionValueId(?productOptionValue $productOptionValue_id): self
    {
        $this->productOptionValue_id = $productOptionValue_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
