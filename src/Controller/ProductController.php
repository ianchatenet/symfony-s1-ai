<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductVariant;
use App\Entity\ProductOptions;
use App\Entity\Tag;
use App\Form\ProductType;
use App\Form\TagType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{


    /**
     * @Route("/", name="product_index", methods={"GET"})
     */
    public function index(): Response
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'controller_name' => 'ProduitsController'
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * @Route("/new", name="product_new", methods={"GET","POST"})
     * 
     */
    public function new(Request $request): Response
    {
           
        $product = new Tag();
        $form = $this->createForm(TagType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            'controller_name' => 'ProduitsController'
        ]);
    }

    /**
     * @Route("/{id}", name="product_show", methods={"GET"})
     */
    public function show($id): Response
    {
        $product=$this->getDoctrine()->getRepository(Product::class)->findOneBy(
            ['id'=>$id]
        );

        $tags = $this->getDoctrine()->getRepository(Tag::class)->findBy(
            ['product'=>$id]
        );

        return $this->render('product/show.html.twig', [
            'product' => $product,
            'controller_name' => 'ProduitsController',
            'tags'=>$tags
        ]);
    }

    /**
     * @IsGranted("ROLE_ADMIN")
     * 
     * @Route("/{id}/edit", name="product_edit", methods={"GET","POST"})
     * 
     */
    public function edit(Request $request, Product $product): Response
    {   
        $product = new Tag();
        $form = $this->createForm(TagType::class, $product);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_index');
            
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
            'controller_name' => 'ProduitsController'
        ]);
        }   
}
