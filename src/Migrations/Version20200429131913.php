<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429131913 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_options (id INT AUTO_INCREMENT NOT NULL, product_option_value_id_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_1ECE1375400C7B4 (product_option_value_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_option_value (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_variant (id INT AUTO_INCREMENT NOT NULL, product_option_value_id_id INT DEFAULT NULL, product_id_id INT DEFAULT NULL, INDEX IDX_209AA41D5400C7B4 (product_option_value_id_id), INDEX IDX_209AA41DDE18E50B (product_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_options ADD CONSTRAINT FK_1ECE1375400C7B4 FOREIGN KEY (product_option_value_id_id) REFERENCES product_option_value (id)');
        $this->addSql('ALTER TABLE product_variant ADD CONSTRAINT FK_209AA41D5400C7B4 FOREIGN KEY (product_option_value_id_id) REFERENCES product_option_value (id)');
        $this->addSql('ALTER TABLE product_variant ADD CONSTRAINT FK_209AA41DDE18E50B FOREIGN KEY (product_id_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_options DROP FOREIGN KEY FK_1ECE1375400C7B4');
        $this->addSql('ALTER TABLE product_variant DROP FOREIGN KEY FK_209AA41D5400C7B4');
        $this->addSql('DROP TABLE product_options');
        $this->addSql('DROP TABLE product_option_value');
        $this->addSql('DROP TABLE product_variant');
    }
}
