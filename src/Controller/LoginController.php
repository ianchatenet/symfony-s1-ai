<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginController extends AbstractController
{

    private $security;

    private $passwordEncoder;

    public function __construct(Security $security, UserPasswordEncoderInterface $passwordEncoder)
    {
        
        $this->security = $security;
        $this->passwordEncoder = $passwordEncoder;
    }


    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/signin",name="signin")
     */
    public function signin(Request $request){

        $user = $this->security->getUser();
        $userD = new User();
        
        if($user){
            return $this->render('security/security.html.twig');
        }

        $form = $this->createForm(UserType::class, $userD);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $userD->setPassword($this->passwordEncoder->encodePassword(
                $userD,
                $userD->getPassword()
            ));
            $entityManager->persist($userD);
            $entityManager->flush();

            return $this->redirectToRoute('product_index');
        }

        return $this->render('security/signin.html.twig', [
            'user' => $userD,
            'form' => $form->createView(),
            'controller_name' => 'ProduitsController'
        ]);
    }
}
