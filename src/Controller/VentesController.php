<?php

namespace App\Controller;

use App\Repository\SaleRepository;
use App\Repository\SaleProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class VentesController extends AbstractController
{

    /**
     * @var SaleRepository
     */
    private $repository;

    /**
     * @var SaleProductRepository
     */
    private $sp;

    

    public function __construct(SaleRepository $repository, SaleProductRepository $sp){
        $this->repository=$repository;
        $this->sp=$sp;
    }

    /**
     * @Route("/ventes", name="ventes")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        

        $sales=$this->repository->findAll();
        $pagination = $paginator->paginate(
            $sales,
            $request->query->getInt('page', 1),
            30
        );

        return $this->render('ventes/index.html.twig', [
            'controller_name' => 'VentesController',
            "pagination"=>$pagination
        ]);

        $pagination->setCustomParameters([
            'position'=>'centered',
            'rounded'=>true,
        ]);
    }

     /**
     * @Route("/ventes/{id}", name="vente_show")
     */
    public function show($id)
    {
        
        $sales=$this->sp->findBy(['sale' => $id]);
        
        return $this->render('ventes/show.html.twig', [
            'controller_name' => 'VentesController',
            'sales'=>$sales
        ]);
    }
}